import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  public continuaJogando = true;
  public tipoResultado: string;

  public encerrarJogo(tipo: string) {
    this.continuaJogando = false;
    this.tipoResultado = tipo;
  }

  public restart() {
    this.continuaJogando = true;
    this.tipoResultado = undefined;
  }
}
