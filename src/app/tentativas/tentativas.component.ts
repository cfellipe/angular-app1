import { Coracao } from './../shared/coracao.model';
import { Component, OnInit, Input, OnChanges } from '@angular/core';

@Component({
  selector: 'app-tentativas',
  templateUrl: './tentativas.component.html',
  styleUrls: ['./tentativas.component.css']
})
export class TentativasComponent implements OnInit, OnChanges {

  public coracaoArray: Array<Coracao> = [
    new Coracao(true), new Coracao(true), new Coracao(true)
  ];

  @Input() public tentativas: number;

  constructor() {

   }

  ngOnInit() {
  }

  ngOnChanges() {

    if (this.tentativas !== this.coracaoArray.length) {

      const i = (this.coracaoArray.length - this.tentativas) - 1;
      this.coracaoArray[i].cheio = false;
   }

  }


}
