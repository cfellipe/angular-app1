import { Frase } from '../shared/frase.model';

export const FRASES: Array<Frase> = [
    { fraseEng: 'I like to learn', frasePtbr: 'Eu gosto de aprender' },
    { fraseEng: 'I watch tv', frasePtbr: 'Eu assisto tv' },
    { fraseEng: 'I eat bread', frasePtbr: 'Eu como pão' },
    { fraseEng: 'I love you', frasePtbr: 'Eu amo você' }
];
