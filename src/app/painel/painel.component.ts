import { ProgressoComponent } from './../progresso/progresso.component';
import { Component, OnInit, EventEmitter, Output, OnDestroy } from '@angular/core';
import { Frase } from '../shared/frase.model';
import { FRASES } from './frases-mock';

@Component({
  selector: 'app-painel',
  templateUrl: './painel.component.html',
  styleUrls: ['./painel.component.css']
})
export class PainelComponent implements OnInit, OnDestroy {

  public frase: Array<Frase> = FRASES;
  public instrucao = 'Traduza a frase:';
  public resposta: string;
  public rodada = 0;
  public fraseRodada: Frase;
  public progressoFrase: number;
  public tentativas = 3;
  @Output() public encerrarJogo: EventEmitter<string> = new EventEmitter();

  constructor() {

  this.progressoFrase = 0 ;
  this.atualizaRodada();

   }

  ngOnInit() {

  }

  ngOnDestroy() {

  }

  verificaResposta() {

    if (this.resposta.toLowerCase === this.fraseRodada.frasePtbr.toLowerCase) {
      this.rodada++;
      this.progressoFrase += (100 / this.frase.length);
      this.atualizaRodada();

      if (this.progressoFrase === 100) {
        this.encerrarJogo.emit('Vitoria');
      }

    } else {

      if (this.tentativas > -1) {
        this.tentativas --;
      }

      if (this.tentativas === 0) {
        this.encerrarJogo.emit('Derrota');
      }
    }
  }


  atualizaResposta(resposta: Event) {
    this.resposta = (<HTMLInputElement>resposta.target).value;

  }
  public atualizaRodada() {
    this.fraseRodada = this.frase[this.rodada];
    this.resposta = '';
  }

}
